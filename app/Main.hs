{-# OPTIONS_GHC -Wall #-}

module Main where

import Graphics.Primitive.Rasterize.Geometry
import Graphics.Primitive.Rasterize.ParametricCurves

import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT
import Control.Monad

resize :: Size -> IO ()
resize s@(Size w h) = do
    viewport $= (Position 0 0, s)
    loadIdentity
    ortho (-w') w' (-h') h' (-1.0) 1.0
    where
        w' = realToFrac w / 200.0
        h' = realToFrac h / 200.0

disp :: IO ()
disp = do
    clear [ColorBuffer]
    color (Color3 0 0 0 :: Color3 GLdouble)
    pointSize $= 1.0
    --renderPrimitive Points {-Lines-} $ forM_ line $ \v -> vertex $ uncurry Vertex2 v
    --renderPrimitive Points $ forM_ hermiteCurve (vertex.(uncurry Vertex2)) -- $ \v -> vertex $ uncurry Vertex2 v
    renderPrimitive Points $ forM_ bezier (vertex.(uncurry Vertex2)) -- ((uncurry Vertex2) vertex)
    flush
    where
        --bezier = quadraticBezier (-1.0, -1.66) (1.10, -1.88) (0.04, 0.86) 0.001 0 1
        bezier = cubicBezier (1, 0) (1, 1) (-1, 1) (-1, 0) 0.001 0 1
        -- hermiteCurve = hermite (1, 0) (0, 1) (-1, 0) (0, -1) 0.001 (-2) 2
        --line = straightLine (0, -200) (200, 300) 0.01

main :: IO ()
main = do
    (progname, _) <- getArgsAndInitialize
    initialDisplayMode $= [RGBAMode]
    _ <- createWindow progname
    clearColor $= Color4 1 1 1 1
    windowTitle $= "Cubic Bezier curve"
    displayCallback $= disp
    reshapeCallback $= Just resize
    mainLoop
