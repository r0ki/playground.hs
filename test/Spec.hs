module Main where

import RNumeric.DFT (dft, idft)
import Data.Complex (Complex ((:+)), realPart, imagPart)
import Data.Tuple.Extra (dupe, (***))
import Test.HUnit (Test (TestList), (~:), (@?=), runTestText, putTextToHandle)
import System.IO (stderr)
import Control.Monad (void)

runTest :: Test -> IO ()
runTest = void.runTestText (putTextToHandle stderr False)

c2zero :: (Fractional a, Ord a) => [Complex a] -> [Complex a]
c2zero = map (uncurry (:+).(exc realPart *** exc imagPart).dupe)
    where
        exc part val | abs (part val) < 1.0e-10 = 0 | otherwise = part val

testList :: Test
testList = let f = map abs.c2zero; l = map (realToFrac.sin.(pi / 2 *)) [0..7] in
    TestList [
        "dct test: " ~: (f l) @?= ((map abs).idft.dft) l
    ]

main :: IO ()
main = runTest testList 
