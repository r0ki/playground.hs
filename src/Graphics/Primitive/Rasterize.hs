{-|
Module      : Graphics.Primitive.Rasterize
Description : Naive modules about rasterize.
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module Graphics.Primitive.Rasterize (
    module Graphics.Primitive.Rasterize.Geometry,
    module Graphics.Primitive.Rasterize.ParametricCurves
) where

import Graphics.Primitive.Rasterize.Geometry
import Graphics.Primitive.Rasterize.ParametricCurves
