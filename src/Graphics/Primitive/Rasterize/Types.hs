{-|
Module      : Graphics.Primitive.Rasterize.Types
Description : Definition of types used in the Graphics module
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module Graphics.Primitive.Rasterize.Types (
    Float'
) where

import Graphics.UI.GLUT

type Float' = GLfloat
