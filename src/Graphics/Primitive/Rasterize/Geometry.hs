{-|
Module      : Graphics.Primitive.Rasterize.Geometry
Description : Naive modules about geometry
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module Graphics.Primitive.Rasterize.Geometry (
    straightLine
) where

import Graphics.Primitive.Rasterize.Types
import Data.Function
import Data.Fixed
import Data.Char
import Control.Arrow
import Control.Monad (join)
import Data.Bifunctor (bimap)
import Graphics.UI.GLUT

data Status = GivenX | GivenY | NoGiven deriving Show

roundPoint1 :: Real a => a -> Deci
roundPoint1 x
    | realToFrac ((realToFrac (realToFrac x :: Centi) - realToFrac (realToFrac x :: Deci)) :: Centi) <= 0.04 = realToFrac x :: Deci
    | otherwise = (realToFrac x + 0.1) :: Deci

straightLinef :: (Int, Int) -> (Int, Int) -> Either (Float', Status) (Int -> Float', Status)
straightLinef (x0, y0) (x1, y1)
    | x0 == x1 = Left (realToFrac x0 :: Float', NoGiven)
    | m >= 1 = Right (fny, GivenY)
    | otherwise = Right (fnx, GivenX)
    where
        m = realToFrac (y1 - y0) / realToFrac (x1 - x0)
        fnx x = (realToFrac $ roundPoint1 $ m * realToFrac x - m * realToFrac x0 + realToFrac y0) :: Float'
        fny y = (realToFrac $ roundPoint1 $ realToFrac y * (1 / m) - realToFrac y0 * (1 / m) + realToFrac x0) :: Float'

-- | The straight line connecting two coordinates is drawn at the specified density.
straightLine :: (Int, Int) -> (Int, Int) -> Float' -> [(Float', Float')]
straightLine p1 p2 density = case straightLinef p1 p2 of
    Left (fl, NoGiven) -> zip (replicate ylen $ f fl) $ take ylen $ iterate (\p -> realToFrac (p + density) :: Float') ((realToFrac (snd p1) / (1 / density)) :: Float')
        where
            ylen = 1 + snd p2
    Right (fs, GivenX) ->  map (dp . (f &&& fs)) [fst p1 .. fst p2] 
    Right (fs, GivenY) ->  map (dp . (fs &&& f)) [snd p1 .. snd p2]  
    where
        f p = realToFrac p :: Float'
        dp = join bimap (/ 100)
