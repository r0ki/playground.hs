{-|
Module      : Graphics.Primitive
Description : Naive modules about graphics.
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module Graphics.Primitive (
    module Graphics.Primitive.Rasterize
) where

import Graphics.Primitive.Rasterize
