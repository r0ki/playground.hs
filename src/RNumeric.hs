{-|
Module      : RNumeric
Description : My original numeric modules
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module RNumeric (
    module RNumeric.DFT
) where

import RNumeric.DFT
