{-|
Module      : RNumeric.DFT
Description : RNumeric DFT modules
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module RNumeric.DFT (
    dft, idft
) where

import Data.List (foldl')
import Data.Complex (Complex ((:+)), realPart, imagPart)
import Data.Tuple.Extra (dupe, first, second, (***))

-- | dft
dft :: [Complex Double] -> [Complex Double]
dft xs = let len = length xs in 
        map (uncurry (:+).(exc realPart *** exc imagPart).dupe.flip (go len) xs) [0..pred len]
            where
                exc part val | abs (part val) < 1.0e-10 = 0 | otherwise = part val
                go len x xs = let j = 0 :+ 1 in
                    foldl' (+) 0 .flip fmap [0..pred len] $
                        uncurry (*).second (exp.negate.(/ fromIntegral len).(2 * pi * j * fromIntegral x *).fromIntegral).first (xs !!).dupe

-- | idft
idft :: [Complex Double] -> [Complex Double]
idft = uncurry fmap.second dft.first (flip (/).fromIntegral.length).dupe
