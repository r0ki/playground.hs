{-|
Module      : ReinventTheWheel.Data
Description : basic reinvent the wheel modules
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module ReinventTheWheel.Data (
    module ReinventTheWheel.Data.Array
) where

import ReinventTheWheel.Data.Array
