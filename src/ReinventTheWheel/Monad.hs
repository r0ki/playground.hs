{-|
Module      : ReinventTheWheel.Monad
Description : monadic reinvent the wheel modules
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module ReinventTheWheel.Monad (
    replicateM, 
    replicateM_,
    forM, 
    forM_,
    when, 
    unless
) where

import Data.Function (fix)
import Control.Applicative ((<$>), (<*>))
import Control.Monad hiding (replicateM, replicateM_, forM, forM_, when, unless)

-- | replicateM
replicateM :: Applicative m => Int -> m a -> m [a]
replicateM 0 _ = pure []
replicateM n act = (:) <$> act <*> replicateM (n-1) act

-- | replicateM_
replicateM_ :: Applicative m => Int -> m a -> m ()
replicateM_ 0 _ = pure ()
replicateM_ c fn = fn *> replicateM_ (c - 1) fn

-- | forM
forM :: Monad m => [a] -> (a -> m b) -> m [b]
forM [] _ = return []
forM (x:xs) fn = (:) <$> fn x <*> forM xs fn

-- | forM_
forM_ :: Monad m => [a] -> (a -> m b) -> m ()
forM_ [] _ = return ()
forM_ xs fn = forM xs fn >> return ()

-- | when
when :: Monad m => Bool -> m () -> m ()
when True fn = fn
when False _ = return ()

-- | unless
unless :: Monad m => Bool -> m () -> m ()
unless = when.not
