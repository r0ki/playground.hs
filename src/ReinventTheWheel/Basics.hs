{-|
Module      : ReinventTheWheel.Basics
Description : basic reinvent the wheel modules
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module ReinventTheWheel.Basics (
    fib,
    fact,
    rot13,
    eratosthenese,
    bubbleSort,
    insertionSort,
    mergeSort,
    quickSort,
    some2dec,
    dec2some,
    shuffle,
    isSorted,
    bogoSort
) where

-- This is the practice code that I went while reading this artircle
-- https://qiita.com/7shi/items/145f1234f8ec2af923ef

import Prelude hiding (foldl', foldr')
import System.Random (getStdRandom, randomR)
import Data.Bits (shiftL, shiftR)
import Data.Char

emptyListErr :: a
emptyListErr = errorWithoutStackTrace "empty list"

zipWith' :: (a -> a -> a) -> [a] -> [a] -> [a]
zipWith' _ _ [] = []
zipWith' _ [] _ = []
zipWith' binaryFn (x:xs) (y:ys) = x `binaryFn` y : zipWith' binaryFn xs ys

-- | generate fibonacchi sequence
fib :: Int -> [Int]
fib n = take' n fib'
    where 
        fib' = 0 : 1 : zipWith' (+) fib' (tail fib')

{-# INLINE foldl' #-}
foldl' :: (b -> a -> b) -> b -> [a] -> b
foldl' _ bp [] = bp
foldl' binaryFn bp (x:xs) = foldl' binaryFn bp xs `binaryFn` x

{-# INLINE foldr' #-}
foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' _ bp [] = bp
foldr' binaryFn bp (x:xs) = x `binaryFn` foldr' binaryFn bp xs

sum' :: Num a => [a] -> a
sum' = foldr' (+) 0

product' :: Num a => [a] -> a
product' = foldr' (*) 1

take' :: Int -> [a] -> [a]
take' 0 _ = []
take' n (x:xs) = x : take' (n - 1) xs

drop' :: Int -> [a] -> [a]
drop' 0 xs = xs
drop' n (_:xs) = drop' (n - 1) xs

reverse' :: [a] -> [a]
reverse' [] = []
reverse' xs = foldl' (\xxs y -> xxs ++ [y]) [] xs

-- | generate factorial sequence
fact :: Int -> Int
fact n = product' [1..n]

perpPoint (p, q) (a, b, c) = (x, y)
    where
        d = b * p - a * q
        x = (a * c + b * d) / (a * a + b * b)
        y = (b * c - a * d) / (a * a + b * b)

-- | generate rot13 string
rot13 :: String -> String
rot13 = map $ \c -> case () of 
    _ | c >= 'A' && c <= 'M' || c >= 'a' && c <= 'm' -> chr $ ord c + 13
      | c >= 'N' && c <= 'Z' || c >= 'n' && c <= 'z' -> chr $ ord c - 13
      | otherwise -> c

-- | sorting the list by bubble sort
bubbleSort :: Ord a => (a -> a -> Bool) -> [a] -> [a]
bubbleSort _ [] = []
bubbleSort op xs = last (bubble xs) : bubbleSort op (init $ bubble xs)
    where
        bubble [x] = [x]
        bubble (x:y:ys)
            | x `op` y = y : bubble (x:ys)
            | otherwise = x : bubble (y:ys)

-- | sorting the list by insert sort
insertionSort :: Ord a => (a -> a -> Bool) -> [a] -> [a]
insertionSort _ [] = []
insertionSort op (x:xs) = insert x $ insertionSort op xs
    where
        insert x [] = [x]
        insert x (y:ys)
            | x `op` y = x:y:ys
            | otherwise = y : insert x ys

-- | sorting the list by merge sort
mergeSort :: Ord a => (a -> a -> Bool) -> [a] -> [a]
mergeSort _ [x] = [x]
mergeSort op xs = merge (mergeSort op $ take' n xs) (mergeSort op $ drop' n xs)
    where
        n = length xs `div` 2
        merge xs [] = xs
        merge [] ys = ys
        merge (x:xs) (y:ys)
            | x `op` y = x : merge xs (y:ys)
            | otherwise = y : merge (x:xs) ys

-- | sorting the list by quick sort
quickSort :: Ord a => (a -> a -> Bool) -> [a] -> [a]
quickSort _ [] = []
quickSort op (x:xs) = quickSort op [l | l <- xs, x `op` l] ++ [x] ++ quickSort op [r | r <- xs, not $ x `op` r]

-- | generate prime numbers by sieve of eratosthenese
eratosthenese :: Int -> [Int]
eratosthenese n = take n $ sieve [2..]
    where
        sieve [] = []
        sieve (p:xs) = p : sieve [x | x <- xs, x `mod` p /= 0]

rightTraiangleSides :: Int -> [(Int, Int, Int)]
rightTraiangleSides n = [(a, b, c) | a <- [1..n], b <- [a..n], c <- [b..n], a * a + b * b == c * c]

-- | The function that converts a certain radix integer to an integer value with radix 10.
some2dec :: Int -> String -> Int
some2dec _ [] = 0
some2dec base (x:xs) = digitToInt x * 1 `shiftL` (length xs * shifts) + some2dec base xs
    where
        shifts = if base > 2 then base `shiftR` 2 else 1

-- | The function to convert an integer value with 10 as a radix to a character string whose arbitrary value is a radix.
-- | Radix not supported by intToDigit is not supported.
dec2some :: Int -> Int -> String
dec2some base n
    | n > 0 = dec2some base (n `div` base) ++ [intToDigit (n `mod` base)]
    | otherwise = []

-- | The Fisher–Yates shuffle function.
shuffle :: Ord a => [a] -> IO [a]
shuffle [x] = return [x]
shuffle xs = do
    r <- getStdRandom $ randomR (0, length xs - 1)
    lo <- shuffle $ take r xs ++ drop (r + 1) xs
    return $ xs !! r : lo

-- | The function to check whether sorting is done.
isSorted :: Ord a => (a -> a -> Bool) -> [a] -> Bool
isSorted _ [] = True
isSorted _ [_] = True
isSorted op (x:y:ys) = (x `op` y) && isSorted op (y:ys)

-- | sorting? the list by bogo sort 
bogoSort :: Ord a => (a -> a -> Bool) -> [a] -> IO [a]
bogoSort op xs = do
    sed <- shuffle xs
    if isSorted op sed then return sed else bogoSort op xs
