{-|
Module      : ReinventTheWheel.Data.Array
Description : basic reinvent the wheel modules
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module ReinventTheWheel.Data.Array (
    module ReinventTheWheel.Data.Array.MArray
) where

import ReinventTheWheel.Data.Array.MArray
