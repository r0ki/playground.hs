{-|
Module      : ReinventTheWheel.Data.Array.MArray
Description : basic reinvent the wheel modules
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module ReinventTheWheel.Data.Array.MArray (
    modifyArray,
    modifyArray_
) where

import Control.Monad (fmap)
import Data.Array.MArray (MArray, Ix, readArray, writeArray)

-- | Modify an element from a mutable array
{-# INLINE modifyArray #-}
modifyArray :: (MArray a e m, Ix i) => a i e -> i -> (e -> e) -> m (a i e)
modifyArray ar idx fn = fmap fn (readArray ar idx) >>= (writeArray ar idx) >> return ar

-- | Modify an element from a mutable array and ignore the results. For a version that doesn't ignore the results see modifyArray 
{-# INLINE modifyArray_ #-}
modifyArray_ :: (MArray a e m, Ix i) => a i e -> i -> (e -> e) -> m ()
modifyArray_ ar idx fn = modifyArray ar idx fn >> return ()
