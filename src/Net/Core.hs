{-|
Module      : Net.Core
Description : The core modules of the basic utilities for network
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module Net.Core (
    ipv4NetAddress
) where

import Data.Bits (shiftL, shiftR, (.&.))
import Data.Word (Word32)

-- | The function to calculate network address from address and subnet mask value.
ipv4NetAddress :: String -> Word32 -> String
ipv4NetAddress [] _ = []
ipv4NetAddress address subnetMask = ipv4NetAddress' (f address) ((0xffffffff :: Word32) `shiftL` (32 - (fromIntegral subnetMask :: Int)))
    where
        f = span (/= '.')
        ipv4NetAddress' (x, []) mask =  show $ ((.&.) mask (read x :: Word32) `shiftL` 24) `shiftR` 24
        ipv4NetAddress' (x, _:ys) mask = show ((.&.) mask ((read x :: Word32) `shiftL` 24) `shiftR` 24) ++ ['.'] ++ ipv4NetAddress' (f ys) (mask `shiftL` 8)
