{-|
Module      : Graphics
Description : Basic utilities for network
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module Net (
    module Net.Core
) where

import Net.Core
