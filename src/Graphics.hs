{-|
Module      : Graphics
Description : Naive modules about graphics.
Copyright   : (c) Roki, 2018
License     : MIT
Maintainer  : falgon53@yahoo.co.jp
Stability   : experimental
Portability : POSIX
-}

module Graphics (
    module Graphics.Primitive
) where

import Graphics.Primitive
